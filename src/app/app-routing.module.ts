import { MedicalformComponent } from './medicalform/medicalform.component';
import { EngineDataComponent } from './information/engine-data/engine-data.component';
import { PassengerDataComponent } from './information/passenger-data/passenger-data.component';
import { FuelComponent } from './information//fuel/fuel.component';
import { ZeroFuelComponent } from './information/zero-fuel/zero-fuel.component';
import { DelayComponent } from './information/delay/delay.component';
import { RemarksComponent } from './information/remarks/remakrs.component';
import { PilotDataComponent } from './information/pilot-data/pilot-data.component';
import { FlightDataComponent } from './information/flight-data/flight-data.component';
import { CruiseDataComponent } from './information/cruise-data/cruise-data.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { PopupComponent } from './information/aircraft/popup/popup.component';
import { HeaderDataComponent } from './information/header-data/header-data.component';
import { MainComponent } from './information/main/main.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inforform',
    pathMatch: 'full',
  },
  {
    path: 'inforform',
    component: MainComponent,
  },
  {
    path: 'header',
    component: HeaderDataComponent,
  },
  {
    path: 'popup',
    component: PopupComponent,
  },
  {
    path: 'cruise',
    component: CruiseDataComponent,
  },
  {
    path: 'pilot',
    component: PilotDataComponent,
  },
  {
    path: 'flight',
    component: FlightDataComponent,
  },
  {
    path: 'remarks',
    component: RemarksComponent,
  },
  {
    path: 'delay',
    component: DelayComponent,
  },
  {
    path: 'zero-fuel-weight',
    component: ZeroFuelComponent,
  },
  {
    path: 'fuel',
    component: FuelComponent,
  },
  {
    path: 'passenger',
    component: PassengerDataComponent,
  },
  {
    path: 'engine',
    component: EngineDataComponent,
  },
  {
    path: 'medicalform',
    component: MedicalformComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
