import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';

@Component({
  selector: 'app-delay',
  templateUrl: './delay.component.html',
  styleUrls: ['./delay.component.scss']
})
export class DelayComponent implements OnInit {

  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }

}