import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';


@Component({
  selector: 'app-engine-data',
  templateUrl: './engine-data.component.html',
  styleUrls: ['./engine-data.component.scss']
})
export class EngineDataComponent implements OnInit {

  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }
}
