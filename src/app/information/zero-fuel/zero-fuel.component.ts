import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';

@Component({
  selector: 'app-zero-fuel',
  templateUrl: './zero-fuel.component.html',
  styleUrls: ['./zero-fuel.component.scss']
})
export class ZeroFuelComponent implements OnInit {

  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }

}