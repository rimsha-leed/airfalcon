import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';

@Component({
  selector: 'app-passenger-data',
  templateUrl: './passenger-data.component.html',
  styleUrls: ['./passenger-data.component.scss']
})
export class PassengerDataComponent implements OnInit {
  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }
}
