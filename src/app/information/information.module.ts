import { EngineDataComponent } from './engine-data/engine-data.component';
import { PassengerDataComponent } from './passenger-data/passenger-data.component';
import { FuelComponent } from './fuel/fuel.component';
import { ZeroFuelComponent } from './zero-fuel/zero-fuel.component';
import { DelayComponent } from './delay/delay.component';
import { RemarksComponent } from './remarks/remakrs.component';
import { MainComponent } from './main/main.component';
import { PilotDataComponent } from './pilot-data/pilot-data.component';
import { CruiseDataComponent } from './cruise-data/cruise-data.component';
import { HeaderDataComponent } from './header-data/header-data.component';
import { FlightDataComponent } from './flight-data/flight-data.component';
import { AircraftComponent } from './aircraft/aircraft.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InformationRoutingModule } from './information.routing.module';
import { AngularMaterialModule } from 'src/shared/angular material components/material.module';


@NgModule({
  declarations: [
AircraftComponent,
FlightDataComponent,
HeaderDataComponent,
CruiseDataComponent,
PilotDataComponent,
MainComponent,
RemarksComponent,
DelayComponent,
ZeroFuelComponent,
FuelComponent,
PassengerDataComponent,
EngineDataComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InformationRoutingModule,
    AngularMaterialModule,

  ],
  providers: [],
})
export class InformationModule {}
