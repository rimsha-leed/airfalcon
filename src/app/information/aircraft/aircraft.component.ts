import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';

@Component({
  selector: 'app-aircraft',
  templateUrl: './aircraft.component.html',
  styleUrls: ['./aircraft.component.scss']
})
export class AircraftComponent implements OnInit {
  

  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  openDialog() {
    this.dialog.open(PopupComponent);
  }

}
