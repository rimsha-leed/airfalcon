import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';


@Component({
  selector: 'app-cruise-data',
  templateUrl: './cruise-data.component.html',
  styleUrls: ['./cruise-data.component.scss']
})
export class CruiseDataComponent implements OnInit {
  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }
}
