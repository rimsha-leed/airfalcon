import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';

@Component({
  selector: 'app-pilot-data',
  templateUrl: './pilot-data.component.html',
  styleUrls: ['./pilot-data.component.scss']
})
export class PilotDataComponent implements OnInit {
  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  openAdd() {
    this.dialog.open(PopupAddComponent)
  }
}
