import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupAddComponent } from './popup-add/popup-add.component';
import { PopupComponent } from './popup/popup.component';

@Component({
  selector: 'app-fuel',
  templateUrl: './fuel.component.html',
  styleUrls: ['./fuel.component.scss']
})
export class FuelComponent implements OnInit {

  constructor(public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  open() {
    this.dialog.open(PopupComponent)
  }
  openAdd() {
    this.dialog.open(PopupAddComponent)
  }
}
