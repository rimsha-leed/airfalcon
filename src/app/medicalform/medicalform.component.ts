import { MedicalFormService } from 'src/shared/medical-form.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-medicalform',
  templateUrl: './medicalform.component.html',
  styleUrls: ['./medicalform.component.scss'],
})
export class MedicalformComponent implements OnInit {
  public data: any;
  public daysLeft = '';
  public 0 = 'null';
  public dnf = 'data-not-found';
  constructor(private api: MedicalFormService, private router: Router) {}

  ngOnInit(): void {
    this.api.getMedicalData(5).subscribe((response: any) => {
      if (response.code === 200) {
        this.data = response.data;
        console.log(response);
        if (parseInt(this.data.license_days_left) <= 0) {
          this.data.license_days_left = 'EXPIRED';
        }
        // if ((this.data) = "") {
        //   this.data = 'EXPIRED';
        // }
      } else {
        console.log('response ===>', response);
      }
    });

    // this.formValue = this.formbuilder.group({
    //   id: [''],
    // });
  }
  postData() {
    // this.MedicalFormModelObj.id = this.formValue.value.id;
    // this.api.postData(this.MedicalFormModelObj)
    // .subscribe(res=>{
    //   console.log(res);
    //   alert("employee addes successfully")
    // },
    // err=>
    // {
    //   alert("something wrong");
    // })
    // }
  }
}
